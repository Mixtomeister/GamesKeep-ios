//
//  AddJuegoController.swift
//  GamesKeep
//
//  Created by Mixtomeister on 19/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class AddJuegoController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, FirebaseAdminDelegate {
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet var btnAceptar:UIButton?
    @IBOutlet var titulo:UITextField?
    @IBOutlet var formato:UISegmentedControl?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.dataSource=self
        self.picker.delegate=self
        btnAceptar?.layer.cornerRadius=8
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (Keeper.metodos.plataformas?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Keeper.metodos.plataformas?[row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func aceptar(){
        Keeper.metodos.gamesPendientes?.append(Game(nombre: (titulo?.text)!, plataforma: (Keeper.metodos.plataformas?[picker.selectedRow(inComponent: 0)])!, estado: 0, formato: (self.formato?.titleForSegment(at: (self.formato?.selectedSegmentIndex)!))!, valoracion: 0, fechaInicio: "", fechaFin:""))
        FirebaseAdmin.metodos.loader?.guardar()
        self.juegoadd()
    }
    
    @IBAction func cancelar() {
        self.juegoadd()
    }
    
    func juegoadd() {
        self.performSegue(withIdentifier: "backMain", sender: self)
    }
}
