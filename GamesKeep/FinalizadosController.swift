//
//  FinalizadosController.swift
//  GamesKeep
//
//  Created by Iván Gálvez Rochel on 5/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class FinalizadosController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseAdmin.metodos.loader?.guardar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Keeper.metodos.gamesFinalizados?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda :CeldaFinalizado = tableView.dequeueReusableCell(withIdentifier: "CeldaFinalizado", for: indexPath) as! CeldaFinalizado
        celda.lblTitulo?.text = Keeper.metodos.gamesFinalizados?[indexPath.row as Int].nombre
        celda.lblFechaI?.text = "Fecha inicio: " + (Keeper.metodos.gamesFinalizados?[indexPath.row as Int].fechaInicio)!
        celda.lblFechaF?.text = "Fecha fin: " + (Keeper.metodos.gamesFinalizados?[indexPath.row as Int].fechaFin)!
        celda.juego = Keeper.metodos.gamesFinalizados?[indexPath.row as Int]
        let plat: String = (Keeper.metodos.gamesFinalizados?[indexPath.row as Int].plataforma)!
        celda.img?.image = Keeper.metodos.imagenes[plat]
        celda.index = indexPath.row
        return celda
    }

}
