//
//  EditarEnCursoController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 21/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class EditarEnCursoController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var txtTitulo:UITextField?
    @IBOutlet var formato:UISegmentedControl?
    @IBOutlet var plataforma:UIPickerView?
    @IBOutlet var fechaI:UIDatePicker?
    @IBOutlet var btnEliminar:UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.plataforma?.dataSource=self
        self.plataforma?.delegate=self
        btnEliminar?.layer.cornerRadius=8
        txtTitulo?.text = Keeper.metodos.juego?.nombre
        if(Keeper.metodos.juego?.formato == "Físico") {
            formato?.selectedSegmentIndex = 0
        } else {
            formato?.selectedSegmentIndex = 1
        }
        plataforma?.selectRow((Keeper.metodos.plataformas?.index(of: (Keeper.metodos.juego?.plataforma)! as String))!, inComponent: 0, animated:true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        fechaI?.setDate(dateFormatter.date(from: (Keeper.metodos.juego?.fechaInicio)!)!, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (Keeper.metodos.plataformas?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Keeper.metodos.plataformas?[row]
    }

    @IBAction func editarDatos() {
        Keeper.metodos.juego?.nombre = txtTitulo?.text
        Keeper.metodos.juego?.plataforma = Keeper.metodos.plataformas?[(plataforma?.selectedRow(inComponent: 0))!]
        Keeper.metodos.juego?.formato = (self.formato?.titleForSegment(at: (self.formato?.selectedSegmentIndex)!))!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        Keeper.metodos.juego?.fechaInicio = dateFormatter.string(from: (fechaI?.date)!)
        self.performSegue(withIdentifier: "menu", sender: self)
    }
    
    @IBAction func eliminarJuego() {
        Keeper.metodos.gamesEnCurso?.remove(at: Keeper.metodos.celda!)
        self.performSegue(withIdentifier: "menu", sender: self)
    }
    
    @IBAction func atras() {
        self.performSegue(withIdentifier: "back", sender: self)
    }
}
