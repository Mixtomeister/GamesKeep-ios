//
//  VistaPendientesController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 21/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class VistaPendientesController: UIViewController {
    @IBOutlet var imagenPlataforma:UIImageView?
    @IBOutlet var lblTitulo:UILabel?
    @IBOutlet var lblFormato:UILabel?
    @IBOutlet var btnComenzar:UIButton?
   
    

    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitulo?.text = Keeper.metodos.juego?.nombre
        lblFormato?.text = Keeper.metodos.juego?.formato
        imagenPlataforma?.image = Keeper.metodos.imagenes[(Keeper.metodos.juego?.plataforma)!]
        imagenPlataforma?.layer.cornerRadius=20
        btnComenzar?.layer.cornerRadius=8
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func comenzarJuego() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        Keeper.metodos.juego?.estado = 1
        Keeper.metodos.juego?.fechaInicio = formatter.string(from: date)
        Keeper.metodos.gamesEnCurso?.append(Keeper.metodos.juego!)
        Keeper.metodos.gamesPendientes?.remove(at: Keeper.metodos.celda!)
        self.volverPantalla()
    }
    
    @IBAction func cancelar() {
        self.volverPantalla()
    }
    
    @IBAction func editar() {
        self.performSegue(withIdentifier: "edit", sender: self)
    }
    
    func volverPantalla() {
        self.performSegue(withIdentifier: "back", sender: self)
    }

}
