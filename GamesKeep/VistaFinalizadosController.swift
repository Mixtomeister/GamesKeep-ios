//
//  VistaFinalizadosController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 21/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class VistaFinalizadosController: UIViewController {
    @IBOutlet var imagenPlataforma:UIImageView?
    @IBOutlet var lblTitulo:UILabel?
    @IBOutlet var lblFormato:UILabel?
    @IBOutlet var lblFechaInicio:UILabel?
    @IBOutlet var lblFechaFin:UILabel?
    @IBOutlet var imgP1:UIImageView?
    @IBOutlet var imgP2:UIImageView?
    @IBOutlet var imgP3:UIImageView?
    @IBOutlet var imgP4:UIImageView?
    @IBOutlet var imgP5:UIImageView?

    override func viewDidLoad() {
        lblTitulo?.text = Keeper.metodos.juego?.nombre
        lblFormato?.text = Keeper.metodos.juego?.formato
        imagenPlataforma?.image = Keeper.metodos.imagenes[(Keeper.metodos.juego?.plataforma)!]
        lblFechaInicio?.text = String("Fecha inicio: " + (Keeper.metodos.juego?.fechaInicio)!)
        lblFechaFin?.text = String("Fecha fin: " + (Keeper.metodos.juego?.fechaFin)!)
        imagenPlataforma?.layer.cornerRadius=20
        printStars()
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func printStars() {
        var valor: Int = 0
        valor = (Keeper.metodos.juego?.valoracion)!
        switch valor {
        case 1:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starno.png")
            imgP3?.image = #imageLiteral(resourceName: "starno.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 2:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starno.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 3:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 4:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starsi.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 5:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starsi.png")
            imgP5?.image = #imageLiteral(resourceName: "starsi.png")
            break
        default: break
        }
    }
    
    @IBAction func cancelar() {
        self.volverPantalla()
    }
    
    @IBAction func editar() {
        self.performSegue(withIdentifier: "edit", sender: self)
    }
    
    func volverPantalla() {
        self.performSegue(withIdentifier: "back", sender: self)
    }

}
