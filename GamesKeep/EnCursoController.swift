//
//  EnCursoController.swift
//  GamesKeep
//
//  Created by Iván Gálvez Rochel on 5/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class EnCursoController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseAdmin.metodos.loader?.guardar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Keeper.metodos.gamesEnCurso?.count)!
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda :CeldaEnCurso = tableView.dequeueReusableCell(withIdentifier: "CeldaEnCurso", for: indexPath) as! CeldaEnCurso
        celda.lblTitulo?.text = Keeper.metodos.gamesEnCurso?[indexPath.row as Int].nombre
        celda.lblFechaI?.text = "Fecha inicio: " + (Keeper.metodos.gamesEnCurso?[indexPath.row as Int].fechaInicio)!
        celda.juego = Keeper.metodos.gamesEnCurso?[indexPath.row as Int]
        let plat: String = (Keeper.metodos.gamesEnCurso?[indexPath.row as Int].plataforma)!
        celda.img?.image = Keeper.metodos.imagenes[plat]
        celda.index = indexPath.row
        return celda
    }

}
