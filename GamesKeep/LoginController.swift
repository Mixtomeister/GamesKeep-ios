//
//  LoginController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 19/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginController: UIViewController, GIDSignInUIDelegate, FirebaseAdminDelegate {
    
    @IBOutlet weak var signInButton: GIDSignInButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        FirebaseAdmin.metodos.delegateLogin(delegate: self)
        if(FirebaseAdmin.metodos.comprobarCuenta() == true){
            GIDSignIn.sharedInstance().signIn()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cargadosDatos(){
        self.performSegue(withIdentifier: "LoginTransition", sender: self)
    }
}
