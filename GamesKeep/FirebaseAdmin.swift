//
//  FirebaseAdmin.swift
//  GamesKeep
//
//  Created by Iván Gálvez Rochel on 5/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import Firebase
import Foundation
import SystemConfiguration


class FirebaseAdmin: NSObject, FirebaseAdminDelegate {
    static let metodos: FirebaseAdmin = FirebaseAdmin()
    var databaseRef:DatabaseReference?
    var delegateLogin:FirebaseAdminDelegate?
    var uid:String?
    var loader: Loader? = Loader()
    
    func checkCon() {
        Database.database().reference(withPath: ".info/connected").observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false {
                self.loader?.internet = true
            } else {
                self.loader?.internet = false
            }
        })
    }
    
    func configureFirebase() {
        FirebaseApp.configure()
        self.databaseRef = Database.database().reference()
        checkCon()
    }
    
    func delegateLogin(delegate: FirebaseAdminDelegate) {
        delegateLogin = delegate
    }
    
    func iniciarSesion(login: AuthCredential){
        Auth.auth().signIn(with: login){ (user, error) in
            self.uid = Auth.auth().currentUser?.uid
            self.delegateLogin?.cargadosDatos!()
        }
    }
    
    func cerrarSesion() {
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func comprobarCuenta() -> Bool {
        if(Auth.auth().currentUser?.uid == nil){
            return false
        }else{
            return true
        }
    }
    
    func bajarImg(URL: String, nombre: String){
        Storage.storage().reference().child(URL).getData(maxSize: 1 * 1024 * 1024){(data, error) -> Void in
            if (error != nil) {
            } else {
                Keeper.metodos.imagenes[nombre] = UIImage(data: data!)
            }
        }
    }
    
    func cargarGames(){
        Keeper.metodos.gamesEnCurso = Array()
        Keeper.metodos.gamesPendientes = Array()
        Keeper.metodos.gamesFinalizados = Array()
        databaseRef?.child("usuarios/" + uid! + "/juegos").observeSingleEvent(of: .value, with: { (snapshot) in
            if(snapshot.exists()){
                let data : Array<AnyObject> = (snapshot.value as? Array<AnyObject>)!
                Keeper.metodos.ultimoIndex = data.count
                for i in data{
                    if(i["estado"] as! Int == 0){
                        Keeper.metodos.gamesPendientes?.append(Game(nombre: i["nombre"] as! String, plataforma: i["plataforma"] as! String, estado: i["estado"] as! Int, formato: i["formato"] as! String, valoracion: 0, fechaInicio: "", fechaFin:""))
                    }else if(i["estado"] as! Int == 1){
                        Keeper.metodos.gamesEnCurso?.append(Game(nombre: i["nombre"] as! String, plataforma: i["plataforma"] as! String, estado: i["estado"] as! Int, formato: i["formato"] as! String, valoracion: 0, fechaInicio: i["fechaInicio"] as! String, fechaFin:""))
                    }else if(i["estado"] as! Int == 2){
                        Keeper.metodos.gamesFinalizados?.append(Game(nombre: i["nombre"] as! String, plataforma: i["plataforma"] as! String, estado: i["estado"] as! Int, formato: i["formato"] as! String, valoracion: i["valoracion"] as! Int, fechaInicio: i["fechaInicio"] as! String, fechaFin: i["fechaFin"] as! String))
                    }
                }
            }else{
                Keeper.metodos.ultimoIndex = 0
            }
            self.cargarPlataformas()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func cargarPlataformas() {
        databaseRef?.child("plataformas").observeSingleEvent(of: .value, with: { (snapshot) in
            let data: Array<AnyObject> = (snapshot.value as? Array<AnyObject>)!
            for i in data{
                Keeper.metodos.plataformas?.append(i["nombre"] as! String)
                self.bajarImg(URL: i["url"] as! String, nombre: i["nombre"] as! String)
            }
            self.loader?.guardarLocal()
            self.delegateLogin?.cargadosDatos!()
        }){ (error) in
            print(error.localizedDescription)
        }
    }
    
    func addString(index : Int, value: String, rama: String) {
        databaseRef?.child("usuarios/" + uid! + "/juegos/" + String(index) + "/" + rama).setValue(value)
    }
    
    func addInt(index : Int, value: Int, rama: String) {
        databaseRef?.child("usuarios/" + uid! + "/juegos/" + String(index) + "/" + rama).setValue(value)
    }
    
}

@objc protocol FirebaseAdminDelegate{
    @objc optional func cargadosDatos()
    @objc optional func juegoadd()
}

