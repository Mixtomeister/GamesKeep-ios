//
//  Keeper.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 5/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class Keeper: NSObject {
    static let metodos: Keeper = Keeper()
    var plataformas: Array<String>? = Array()
    var gamesEnCurso: Array<Game>? = Array()
    var gamesPendientes: Array<Game>? = Array()
    var gamesFinalizados: Array<Game>? = Array()
    var ultimoIndex:Int?
    var juego: Game?
    var celda: Int?
    var imagenes: [String:UIImage] = [:]
}
