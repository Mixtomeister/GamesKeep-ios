//
//  VistaEnCursoController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 21/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class VistaEnCursoController: UIViewController {
    @IBOutlet var imagenPlataforma:UIImageView?
    @IBOutlet var lblTitulo:UILabel?
    @IBOutlet var lblFormato:UILabel?
    @IBOutlet var lblFechaInicio:UILabel?
    var valora: Int = 0
    @IBOutlet var btnFinalizar:UIButton?
    @IBOutlet var imgP1:UIImageView?
    @IBOutlet var imgP2:UIImageView?
    @IBOutlet var imgP3:UIImageView?
    @IBOutlet var imgP4:UIImageView?
    @IBOutlet var imgP5:UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitulo?.text = Keeper.metodos.juego?.nombre
        lblFormato?.text = Keeper.metodos.juego?.formato
        imagenPlataforma?.image = Keeper.metodos.imagenes[(Keeper.metodos.juego?.plataforma)!]
        lblFechaInicio?.text = String("Fecha inicio: " + (Keeper.metodos.juego?.fechaInicio)!)
        btnFinalizar?.layer.cornerRadius=8
        btnFinalizar?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        imagenPlataforma?.layer.cornerRadius=20
        let p1 = UITapGestureRecognizer(target: self, action: #selector(p1(tapGestureRecognizer:)))
        let p2 = UITapGestureRecognizer(target: self, action: #selector(p2(tapGestureRecognizer:)))
        let p3 = UITapGestureRecognizer(target: self, action: #selector(p3(tapGestureRecognizer:)))
        let p4 = UITapGestureRecognizer(target: self, action: #selector(p4(tapGestureRecognizer:)))
        let p5 = UITapGestureRecognizer(target: self, action: #selector(p5(tapGestureRecognizer:)))
        imgP1?.isUserInteractionEnabled = true
        imgP2?.isUserInteractionEnabled = true
        imgP3?.isUserInteractionEnabled = true
        imgP4?.isUserInteractionEnabled = true
        imgP5?.isUserInteractionEnabled = true
        imgP1?.addGestureRecognizer(p1)
        imgP2?.addGestureRecognizer(p2)
        imgP3?.addGestureRecognizer(p3)
        imgP4?.addGestureRecognizer(p4)
        imgP5?.addGestureRecognizer(p5)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func finalizarJuego() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        Keeper.metodos.juego?.estado = 2
        Keeper.metodos.juego?.valoracion = valora
        Keeper.metodos.juego?.fechaFin = formatter.string(from: date)
        Keeper.metodos.gamesFinalizados?.append(Keeper.metodos.juego!)
        Keeper.metodos.gamesEnCurso?.remove(at: Keeper.metodos.celda!)
        self.volverPantalla()
    }
    
    func p1(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 1
        printStars()
    }
    func p2(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 2
        printStars()
    }
    func p3(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 3
        printStars()
    }
    func p4(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 4
        printStars()
    }
    func p5(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 5
        printStars()
    }
    
    func printStars() {
        switch valora {
        case 1:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starno.png")
            imgP3?.image = #imageLiteral(resourceName: "starno.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 2:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starno.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 3:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 4:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starsi.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 5:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starsi.png")
            imgP5?.image = #imageLiteral(resourceName: "starsi.png")
            break
        default: break
        }
        btnFinalizar?.isEnabled = true
        btnFinalizar?.backgroundColor = #colorLiteral(red: 0.02861191519, green: 0.7141743302, blue: 0.6218090653, alpha: 1)
    }
    
    @IBAction func cancelar() {
        self.volverPantalla()
    }
    
    @IBAction func editar() {
        self.performSegue(withIdentifier: "edit", sender: self)
    }
    
    func volverPantalla() {
        self.performSegue(withIdentifier: "back", sender: self)
    }
    
}
