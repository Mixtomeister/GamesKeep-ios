//
//  Loader.swift
//  GamesKeep
//
//  Created by Mixtomeister on 20/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class Loader: NSObject {
    var internet : Bool?
    
    override init() {
        internet = false
    }
    
    func cargarLocal() {
        Keeper.metodos.gamesPendientes = Array()
        let numPendientes:Int = UserDefaults.standard.value(forKey: "numPendientes") as! Int
        var index: Int = 0
        while index < numPendientes {
            Keeper.metodos.gamesPendientes?.append(Game(array: UserDefaults.standard.value(forKey: "pendientes" + String(index)) as! [String : Any]))
            index = index + 1
        }
        
        Keeper.metodos.gamesEnCurso = Array()
        let numEnCurso:Int = UserDefaults.standard.value(forKey: "numEnCurso") as! Int
        index = 0
        while index < numEnCurso {
            Keeper.metodos.gamesEnCurso?.append(Game(array: UserDefaults.standard.value(forKey: "enCurso" + String(index)) as! [String : Any]))
            index = index + 1
        }
        
        Keeper.metodos.gamesFinalizados = Array()
        let numFinalizados:Int = UserDefaults.standard.value(forKey: "numFinalizados") as! Int
        index = 0
        while index < numFinalizados {
            Keeper.metodos.gamesFinalizados?.append(Game(array: UserDefaults.standard.value(forKey: "finalizados" + String(index)) as! [String : Any]))
            index = index + 1
        }
        
    }
    
    func guardarLocal() {
        UserDefaults.standard.set(Keeper.metodos.gamesPendientes?.count, forKey: "numPendientes")
        UserDefaults.standard.set(Keeper.metodos.gamesEnCurso?.count, forKey: "numEnCurso")
        UserDefaults.standard.set(Keeper.metodos.gamesFinalizados?.count, forKey: "numFinalizados")
        var index: Int = 0
        for i in Keeper.metodos.gamesPendientes!{
            UserDefaults.standard.set(i.toArray(), forKey: "pendientes" + String(index))
            index = index + 1
        }
        
        index = 0
        for i in Keeper.metodos.gamesEnCurso!{
            UserDefaults.standard.set(i.toArray(), forKey: "enCurso" + String(index))
            index = index + 1
        }
        
        index = 0
        for i in Keeper.metodos.gamesFinalizados!{
            UserDefaults.standard.set(i.toArray(), forKey: "finalizados" + String(index))
            index = index + 1
        }
    }
    
    func cargarFirebase() {
        FirebaseAdmin.metodos.cargarGames()
    }
    
    func guardarFirebase() {
        guardarLocal()
        var index: Int = 0
        for i in Keeper.metodos.gamesPendientes! {
            FirebaseAdmin.metodos.addString(index:index, value:i.nombre!, rama: "nombre")
            FirebaseAdmin.metodos.addString(index:index, value:i.plataforma!, rama: "plataforma")
            FirebaseAdmin.metodos.addString(index:index, value:i.formato!, rama: "formato")
            FirebaseAdmin.metodos.addInt(index:index, value:i.estado!, rama: "estado")
            index = index + 1
        }
        index = 0
        for i in Keeper.metodos.gamesEnCurso! {
            FirebaseAdmin.metodos.addString(index:index, value:i.nombre!, rama: "nombre")
            FirebaseAdmin.metodos.addString(index:index, value:i.plataforma!, rama: "plataforma")
            FirebaseAdmin.metodos.addString(index:index, value:i.formato!, rama: "formato")
            FirebaseAdmin.metodos.addInt(index:index, value:i.estado!, rama: "estado")
            FirebaseAdmin.metodos.addString(index:index, value:i.fechaInicio!, rama: "fechaInicio")
            index = index + 1
        }
        
        index = 0
        for i in Keeper.metodos.gamesFinalizados! {
            FirebaseAdmin.metodos.addString(index:index, value:i.nombre!, rama: "nombre")
            FirebaseAdmin.metodos.addString(index:index, value:i.plataforma!, rama: "plataforma")
            FirebaseAdmin.metodos.addString(index:index, value:i.formato!, rama: "formato")
            FirebaseAdmin.metodos.addInt(index:index, value:i.estado!, rama: "estado")
            FirebaseAdmin.metodos.addString(index:index, value:i.fechaInicio!, rama: "fechaInicio")
            FirebaseAdmin.metodos.addString(index:index, value:i.fechaFin!, rama: "fechaFin")
            FirebaseAdmin.metodos.addInt(index:index, value:i.valoracion!, rama: "valoracion")
            index = index + 1
        }
    }
    
    func cargar(){
        if(internet == true){
            cargarFirebase()
        }else{
            cargarLocal()
        }
    }
    
    func guardar(){
        if(internet == true){
            guardarFirebase()
        }else{
            guardarLocal()
        }
    }
}
