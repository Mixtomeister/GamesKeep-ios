//
//  PendientesController.swift
//  GamesKeep
//
//  Created by Iván Gálvez Rochel on 5/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class PendientesController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseAdmin.metodos.loader?.guardar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Keeper.metodos.gamesPendientes?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda :CeldaPendiente = tableView.dequeueReusableCell(withIdentifier: "CeldaPendiente", for: indexPath) as! CeldaPendiente
        celda.lblTitulo?.text = Keeper.metodos.gamesPendientes?[indexPath.row as Int].nombre
        celda.juego = Keeper.metodos.gamesPendientes?[indexPath.row as Int]
        let plat: String = (Keeper.metodos.gamesPendientes?[indexPath.row as Int].plataforma)!
        celda.img?.image = Keeper.metodos.imagenes[plat]
        celda.index = indexPath.row
        return celda
    }

}
