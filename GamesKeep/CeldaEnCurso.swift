//
//  CeldaEnCurso.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 5/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class CeldaEnCurso: UITableViewCell {
    
    @IBOutlet var lblTitulo:UILabel?
    @IBOutlet var lblFechaI:UILabel?
    @IBOutlet var img:UIImageView?
    var juego: Game?
    var index: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        Keeper.metodos.juego = self.juego
        Keeper.metodos.celda = index
    }

}
