//
//  Game.swift
//  GamesKeep
//
//  Created by Mixtomeister on 8/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class Game: NSObject {
    var nombre : String?
    var plataforma : String?
    var estado : Int?
    var formato : String?
    var valoracion : Int?
    var fechaInicio : String?
    var fechaFin : String?
    var imagen: UIImage?
    
    init(nombre:String, plataforma:String, estado:Int, formato:String, valoracion:Int, fechaInicio:String, fechaFin:String) {
        self.nombre = nombre
        self.plataforma = plataforma
        self.estado = estado
        self.formato = formato
        self.valoracion = valoracion
        self.fechaInicio = fechaInicio
        self.fechaFin = fechaFin
    }
    
    init(array:[String:Any]) {
        print(array)
        self.nombre = array["nombre"] as! String?
        self.plataforma = array["plataforma"] as! String?
        self.estado = array["estado"] as! Int?
        self.formato = array["formato"] as! String?
        self.valoracion = array["valoracion"] as! Int?
        self.fechaInicio = array["fechaInicio"] as! String?
        self.fechaFin = array["fechaFin"] as! String?
    }
    
    func toArray() -> NSDictionary {
        let array:[String: Any] = [
            "nombre": self.nombre!,
            "plataforma": self.plataforma!,
            "estado": self.estado!,
            "formato": self.formato!,
            "valoracion": self.valoracion!,
            "fechaInicio": self.fechaInicio!,
            "fechaFin": self.fechaFin!
        ]
        return array as NSDictionary
    }
    
    func setImagen(img: UIImage) {
        imagen = img
    }
}
