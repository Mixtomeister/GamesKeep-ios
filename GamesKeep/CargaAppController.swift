//
//  CargaAppController.swift
//  GamesKeep
//
//  Created by Iván Gálvez Rochel on 22/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class CargaAppController: UIViewController, FirebaseAdminDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseAdmin.metodos.delegateLogin(delegate: self)
        FirebaseAdmin.metodos.loader?.cargarFirebase()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cargadosDatos() {
        self.performSegue(withIdentifier: "loadTr", sender: self)
    }


}
