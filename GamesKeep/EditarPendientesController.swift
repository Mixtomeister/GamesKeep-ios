//
//  EditarPendientesController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 21/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class EditarPendientesController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var txtTitulo:UITextField?
    @IBOutlet var formato:UISegmentedControl?
    @IBOutlet var plataforma:UIPickerView?
    @IBOutlet var btnEliminar:UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.plataforma?.dataSource=self
        self.plataforma?.delegate=self
        btnEliminar?.layer.cornerRadius=8
        txtTitulo?.text = Keeper.metodos.juego?.nombre
        if(Keeper.metodos.juego?.formato == "Físico") {
            formato?.selectedSegmentIndex = 0
        } else {
            formato?.selectedSegmentIndex = 1
        }
        plataforma?.selectRow((Keeper.metodos.plataformas?.index(of: (Keeper.metodos.juego?.plataforma)! as String))!, inComponent: 0, animated:true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (Keeper.metodos.plataformas?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Keeper.metodos.plataformas?[row]
    }
    
    @IBAction func editarDatos() {
        Keeper.metodos.juego?.nombre = txtTitulo?.text
        Keeper.metodos.juego?.plataforma = Keeper.metodos.plataformas?[(plataforma?.selectedRow(inComponent: 0))!]
        Keeper.metodos.juego?.formato = (self.formato?.titleForSegment(at: (self.formato?.selectedSegmentIndex)!))!
        self.performSegue(withIdentifier: "menu", sender: self)
    }
    
    @IBAction func eliminarJuego() {
        Keeper.metodos.gamesPendientes?.remove(at: Keeper.metodos.celda!)
        self.performSegue(withIdentifier: "menu", sender: self)
    }
    
    @IBAction func atras() {
        self.performSegue(withIdentifier: "back", sender: self)
    }

}
