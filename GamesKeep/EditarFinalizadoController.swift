//
//  EditarFinalizadoController.swift
//  GamesKeep
//
//  Created by Jaime García Castán on 21/6/17.
//  Copyright © 2017 Iván Gálvez y Jaime García. All rights reserved.
//

import UIKit

class EditarFinalizadoController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet var txtTitulo:UITextField?
    @IBOutlet var formato:UISegmentedControl?
    @IBOutlet var plataforma:UIPickerView?
    @IBOutlet var fechaI:UIDatePicker?
    @IBOutlet var fechaF:UIDatePicker?
    @IBOutlet var imgP1:UIImageView?
    @IBOutlet var imgP2:UIImageView?
    @IBOutlet var imgP3:UIImageView?
    @IBOutlet var imgP4:UIImageView?
    @IBOutlet var imgP5:UIImageView?
    @IBOutlet var btnEliminar:UIButton?
    var valora: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.plataforma?.dataSource=self
        self.plataforma?.delegate=self
        btnEliminar?.layer.cornerRadius=8
        txtTitulo?.text = Keeper.metodos.juego?.nombre
        valora = (Keeper.metodos.juego?.valoracion)!
        if(Keeper.metodos.juego?.formato == "Físico") {
            formato?.selectedSegmentIndex = 0
        } else {
            formato?.selectedSegmentIndex = 1
        }
        plataforma?.selectRow((Keeper.metodos.plataformas?.index(of: (Keeper.metodos.juego?.plataforma)! as String))!, inComponent: 0, animated:true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        fechaI?.setDate(dateFormatter.date(from: (Keeper.metodos.juego?.fechaInicio)!)!, animated: true)
        let dateFormatterF = DateFormatter()
        dateFormatterF.dateFormat = "dd-MM-yyyy"
        fechaF?.setDate(dateFormatterF.date(from: (Keeper.metodos.juego?.fechaFin)!)!, animated: true)
        let p1 = UITapGestureRecognizer(target: self, action: #selector(p1(tapGestureRecognizer:)))
        let p2 = UITapGestureRecognizer(target: self, action: #selector(p2(tapGestureRecognizer:)))
        let p3 = UITapGestureRecognizer(target: self, action: #selector(p3(tapGestureRecognizer:)))
        let p4 = UITapGestureRecognizer(target: self, action: #selector(p4(tapGestureRecognizer:)))
        let p5 = UITapGestureRecognizer(target: self, action: #selector(p5(tapGestureRecognizer:)))
        imgP1?.isUserInteractionEnabled = true
        imgP2?.isUserInteractionEnabled = true
        imgP3?.isUserInteractionEnabled = true
        imgP4?.isUserInteractionEnabled = true
        imgP5?.isUserInteractionEnabled = true
        imgP1?.addGestureRecognizer(p1)
        imgP2?.addGestureRecognizer(p2)
        imgP3?.addGestureRecognizer(p3)
        imgP4?.addGestureRecognizer(p4)
        imgP5?.addGestureRecognizer(p5)
        printStars()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (Keeper.metodos.plataformas?.count)!
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Keeper.metodos.plataformas?[row]
    }
    
    func p1(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 1
        printStars()
    }
    func p2(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 2
        printStars()
    }
    func p3(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 3
        printStars()
    }
    func p4(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 4
        printStars()
    }
    func p5(tapGestureRecognizer: UITapGestureRecognizer) {
        self.valora = 5
        printStars()
    }
    
    func printStars() {
        switch valora {
        case 1:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starno.png")
            imgP3?.image = #imageLiteral(resourceName: "starno.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 2:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starno.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 3:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starno.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 4:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starsi.png")
            imgP5?.image = #imageLiteral(resourceName: "starno.png")
            break
        case 5:
            imgP1?.image = #imageLiteral(resourceName: "starsi.png")
            imgP2?.image = #imageLiteral(resourceName: "starsi.png")
            imgP3?.image = #imageLiteral(resourceName: "starsi.png")
            imgP4?.image = #imageLiteral(resourceName: "starsi.png")
            imgP5?.image = #imageLiteral(resourceName: "starsi.png")
            break
        default: break
        }
    }


    @IBAction func editarDatos() {
        Keeper.metodos.juego?.nombre = txtTitulo?.text
        Keeper.metodos.juego?.plataforma = Keeper.metodos.plataformas?[(plataforma?.selectedRow(inComponent: 0))!]
        Keeper.metodos.juego?.formato = (self.formato?.titleForSegment(at: (self.formato?.selectedSegmentIndex)!))!
        Keeper.metodos.juego?.valoracion = valora
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        Keeper.metodos.juego?.fechaInicio = dateFormatter.string(from: (fechaI?.date)!)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd-MM-yyyy"
        Keeper.metodos.juego?.fechaFin = dateFormatter2.string(from: (fechaF?.date)!)
        self.performSegue(withIdentifier: "menu", sender: self)
    }
    
    @IBAction func eliminarJuego() {
        Keeper.metodos.gamesFinalizados?.remove(at: Keeper.metodos.celda!)
        self.performSegue(withIdentifier: "menu", sender: self)
    }
    
    @IBAction func atras() {
        self.performSegue(withIdentifier: "back", sender: self)
    }

}
